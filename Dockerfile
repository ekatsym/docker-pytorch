FROM nvidia/cuda:11.3.1-cudnn8-devel-ubuntu20.04

MAINTAINER ekatsym

ENV DEBIAN_FRONTEND=noninteractive
ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

RUN apt update \
 && apt install -y curl git-core python3.8 python3-pip python-is-python3 python3-venv python3-tk libopencv-dev \
 && apt clean \
 && rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade pip
RUN pip install pipenv

RUN mkdir /workspace \
 && cd /workspace \
 && /usr/local/bin/pipenv --python 3.8 \
 && /usr/local/bin/pipenv install torch torchvision torchaudio ipython notebook

WORKDIR /workspace

CMD ["/bin/bash"]
